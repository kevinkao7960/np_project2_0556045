#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <signal.h>

#define MAXLINE 512
#define SERVER_PORT 7888
#define SHELL_TOK_BUFSIZE 64
#define SHELL_TOK_DELIM " \t\r\n"
#define MY_PATH "PATH=bin:."
#define DEFAULT_DIR "/home/mbl/rwg"
#define SWAP( a, b ) { c = a; a = b; b = c; }
#define MAX_PIPE_NUM 2000
#define TEST 1

/* shm define */
#define SHMKEY ((key_t) 7888) /* base value for shmem key */
#define SEMKEY1 ((key_t) 7891) /* client semaphore key */
#define SEMKEY2 ((key_t) 7892) /* server semaphore key */
#define PERMS 0666
#define MAXMESGDATA (4096-16)
#define MESGHDRSIZE ( sizeof(Mesg) - MAXMESGDATA )


typedef struct {
    int id; /* #bytes in mesg_data, can be 0 or > 0 */
    int pid;
    char nickname[50]; /* message type, must be > 0 */
    char ip[50];
}User;

typedef struct {
    int broadFlag;
    int numMesg;
    int numBroad;
    char message[10][1024];
    char broadcast[5][1024];
}Mesg;

typedef struct {
    int srcId;
    int destId;
}IdInfo;

typedef struct{
    char writefifo[10];
    char readfifo[10];
    int writeNum;
    int readNum;
}Fifofile;

typedef struct{
    int i, j;
    int flag;
}CloseFifo;

void err_dump( const char* x );
int readline( int fd, char *ptr, int maxlen );
void shell_loop( int ser_sockfd );
char *shell_read_line( int sockfd );
char **shell_split_line( char *line );
int shell_execute( char **args );
int shell_launch_command( char **args );
int setenvPATH( char **args );
int printenv( char **args );
void redirection( char **args, int filename_pos );
int calArgsLen( char **args );
char **shell_split_pipe( char *line );
int dealpipe( char **commands, int commandNum, int table[][3], int tableRow, int zeroCounterRow, int exclamation );
void initialPipeTable( int table[][3] );
int containNum( char **commands, int commandNum );
int discoverEmptyEntryInTable( int table[][3] );
int findSameCounter( int table[][3], int counter );
void subOneToCounter( int table[][3] );
int findErrNum( char **commands, int commandNum );
void intHandler( int s );
void sysMesgHandler( int s );
void tellAndYellHandler( int s );

User* userinfo;
Mesg** mesgTable;
Fifofile** fifoTable;

char* sysMesg;

int shmid, clisem, servsem;
int sysShmId;
int mesgShmId[30];

int idShm;
IdInfo* idinfo;

int fifoShmId[30];
char com[1024];
CloseFifo* closefifo;
int closefifoNum;

int main(int argc, char const *argv[]) {
    int sockfd, newsockfd, chilen, childpid, ser_port;
    struct sockaddr_in cli_addr, serv_addr;

    if( argv[1] != NULL ){
        ser_port = atoi( argv[1] );
    }
    else{
        ser_port = SERVER_PORT;
    }
    char welcome[3][42] = {"****************************************\n",
    "** Welcome to the information server. **\n",
    "****************************************\n"};

    /* enlarge the fd table */
    struct rlimit r;
    r.rlim_cur=2100;
    r.rlim_max=2500;

    if (setrlimit(RLIMIT_NOFILE,&r)<0){
        fprintf(stderr,"setrlimit error\n");
        exit(1);
    }

    /* add signal function to deal with dead child */
    signal( SIGINT, intHandler );
    signal( SIGUSR1, sysMesgHandler );
    signal( SIGUSR2, tellAndYellHandler );

    /* Create shared memory */
    if( (shmid = shmget(SHMKEY, sizeof(userinfo) * 30, PERMS|IPC_CREAT)) < 0 ){
        err_dump( "server: can't get shared memory" );
    }
    userinfo = (User *) shmat(shmid, NULL, 0);

    /* Create shared memory for system message */
    if( ( sysShmId = shmget( SHMKEY + 1, sizeof(char) * 1024, PERMS|IPC_CREAT )) < 0 ){
        err_dump( "server: can't get system mesg shared memory" );
    }
    sysMesg = (char *) shmat( sysShmId, NULL, 0 );

    /* create mesg table */
    mesgTable = (Mesg**)malloc( sizeof( Mesg* ) * 30 );
    for( int i = 0; i < 30; i++ ){
        if( ( mesgShmId[i] = shmget( SHMKEY + i + 2, sizeof(Mesg) * 30, PERMS|IPC_CREAT )) < 0 ){
            err_dump( "server: can't get system mesg shared memory to mesgShmId" );
        }
        mesgTable[i] = (Mesg *)shmat( mesgShmId[i], NULL, 0 );
    }

    /* create idinfo shared memory */
    if( ( idShm = shmget( SHMKEY + 32, sizeof(IdInfo), PERMS|IPC_CREAT )) < 0 ){
        err_dump( "server: can't get system mesg shared memory" );
    }
    idinfo = (IdInfo *) shmat( idShm, NULL, 0 );

    /* create fifoTable shared memory */
    fifoTable = ( Fifofile** )malloc( sizeof( Fifofile* ) * 30 );
    for( int i = 0; i < 30; i++ ){
        if( ( fifoShmId[i] = shmget( SHMKEY + i + 33, sizeof(Fifofile) * 30, PERMS|IPC_CREAT )) < 0 ){
            err_dump( "server: can't get system mesg shared memory to fifoShmId" );
        }
        fifoTable[i] = (Fifofile *)shmat( fifoShmId[i], NULL, 0 );
    }

    /* create closefifo */
    if( ( closefifoNum = shmget( SHMKEY + 63, sizeof(CloseFifo), PERMS|IPC_CREAT )) < 0 ){
        err_dump( "server: can't get system mesg shared memory to closefifoNum ");
    }
    closefifo = (CloseFifo *)shmat( closefifoNum, NULL, 0);

    /* initialize the struct array */
    for( int i = 0; i < 30; i++ ){
        userinfo[i].id = 0;
        userinfo[i].pid = 0;
    }

    /* initialize the mesgTable */
    for( int i = 0; i < 30; i++ ){
        for( int j = 0; j < 30; j++ ){
            mesgTable[i][j].broadFlag = 0;
            mesgTable[i][j].numMesg = 0;
            mesgTable[i][j].numBroad = 0;
            for( int k = 0; k < 10; k++ ){
                memset( mesgTable[i][j].message[k], '\0', sizeof(mesgTable[i][j].message[k]) );
                if( k < 5 ){
                    memset( mesgTable[i][j].broadcast[k], '\0', sizeof(mesgTable[i][j].broadcast[k]) );
                }
            }
        }
    }

    /* Open a TCP socket ( an Internet stream socket ). */
    if( ( sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
        err_dump("server: can't open stream socket");
    }

    /* Bind local address so that the client can send to us. */
    bzero(( char* ) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    serv_addr.sin_port = htons( ser_port );


    if( bind( sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0 ){
        err_dump("server: can't bind local address");
    }

    listen(sockfd, 30);

    // Create socket and accept
    for(;;){
        chilen = sizeof( cli_addr );
        newsockfd = accept( sockfd, (struct sockaddr*) &cli_addr, &chilen );
        if( newsockfd < 0 ) err_dump("server: accept error");

        if( (childpid = fork()) < 0 ) err_dump("server: fork error");
        else if( childpid == 0 ){
            /* child process */

            /* close original socket */
            close( sockfd );

            /* change stdout to newsockfd */
            dup2( newsockfd, STDOUT_FILENO );
            /* change stderr to newsockfd */
            dup2( newsockfd, STDERR_FILENO );

            /* change to default dir */
            chdir( DEFAULT_DIR );
            chroot( DEFAULT_DIR );

            /* initial the path */
            putenv( MY_PATH );

            /* print the welcome messages */
            for( int i = 0; i < 3; i++ ){
                int n = strlen( welcome[i] );
                if( write( newsockfd, welcome[i], n ) != n )
                err_dump("writen error");
            }

            /* add user info into table */
            for( int i = 0; i < 30; i++ ){
                if( userinfo[i].id == 0 ){
                    userinfo[i].id = i + 1;
                    userinfo[i].pid = getpid();
                    strcpy( userinfo[i].nickname, "(no name)");
                    // strcpy( userinfo[i].ip, inet_ntoa( cli_addr.sin_addr ) );
                    strcpy( userinfo[i].ip, "CGILAB" );
                    strcat( userinfo[i].ip, "/");
                    // char a[1000];
                    // int port = ntohs( cli_addr.sin_port );
                    // sprintf( a, "%d", port );
                    // strcat( userinfo[i].ip, a );
                    strcat( userinfo[i].ip, "511" );
                    break;
                }
            }

            /* send enter messages */
            char enterMsg[1024] = "\0";
            strcat( enterMsg, "*** User \'" );
            for( int i = 0; i < 30; i++ ){
                if( userinfo[i].pid == getpid() ){
                    strcat( enterMsg, userinfo[i].nickname );
                    strcat( enterMsg, "\' entered from " );
                    strcat( enterMsg, userinfo[i].ip );
                    strcat( enterMsg, ". ***\n" );
                    break;
                }
            }
            strcpy( sysMesg, enterMsg );
            // raise( SIGUSR1 );
            for( int i = 0; i < 30; i++ ){
                if( userinfo[i].pid != 0 ){
                    kill( userinfo[i].pid, SIGUSR1 );
                }
            }


            closefifo->i = -1;
            closefifo->j = -1;
            closefifo->flag = 1;

            /* create shell */
            shell_loop( newsockfd );

            exit(0);
        }
        else{
            close( newsockfd ); /* parent process */
            // wait( NULL );
        }
    }
    close( sockfd );

    return 0;
}

// handle signal SIGINT
void intHandler( int s ){
    if( s == SIGINT ){
        shmdt( (void*)userinfo );
        shmctl( shmid, IPC_RMID, NULL );
        shmdt( (void*)sysMesg );
        shmctl( sysShmId, IPC_RMID, NULL );

        for( int i = 0; i < 30; i++ ){
            shmdt( (void*)mesgTable[i] );
            shmctl( mesgShmId[i], IPC_RMID, NULL );
        }

        for( int i = 0; i < 30; i++ ){
            for( int j = 0; j < 30; j++ ){
                // close( fifoTable[i][j].readNum );
                // close( fifoTable[i][j].writeNum );
                unlink( fifoTable[i][j].writefifo );
                // unlink( fifoTable[i][j].writefifo );
            }
        }

        for( int i = 0; i < 30; i++ ){
            shmdt( (void*)fifoTable[i] );
            shmctl( fifoShmId[i], IPC_RMID, NULL );
        }

        shmdt( (void*)idinfo );
        shmctl( idShm, IPC_RMID, NULL );

        shmdt( (void*)closefifo );
        shmctl( closefifoNum, IPC_RMID, NULL );
    }
    exit(0);
}

// handle signal SIGUSR1 ( system mesg )
void sysMesgHandler( int s ){
    if( s == SIGUSR1 ){
        write( STDOUT_FILENO, sysMesg, strlen(sysMesg) );
    }
}

/* handle siganl SIGUSR2 ( tell and yell ) */
void tellAndYellHandler( int s ){
    if( s == SIGUSR2 ){

        int srcId = idinfo->srcId;
        int destId = idinfo->destId;
        int numMesg = mesgTable[srcId][destId].numMesg;
        int numBroad = mesgTable[srcId][destId].numBroad;
        char tellMsg[1024];

        if( mesgTable[srcId][destId].broadFlag == 1 ){
            for( int i = 0; i < numBroad; i++ ){
                memset( tellMsg, '\0', 1024 );
                strcpy( tellMsg, "*** ");
                strcat( tellMsg, userinfo[srcId].nickname );
                strcat( tellMsg, " yelled ***: " );
                strcat( tellMsg, mesgTable[srcId][destId].broadcast[i] );
                write( STDOUT_FILENO, tellMsg, strlen( tellMsg ) );
                write( STDOUT_FILENO, "\n", 1 );
                memset( mesgTable[srcId][destId].broadcast[i], '\0', strlen( mesgTable[srcId][destId].broadcast[i] ) );
            }
            mesgTable[srcId][destId].numBroad = 0;
            mesgTable[srcId][destId].broadFlag = 0;
        }
        else{
            for( int i = 0; i < numMesg; i++ ){
                memset( tellMsg, '\0', 1024 );
                strcpy( tellMsg, "*** " );
                strcat( tellMsg, userinfo[srcId].nickname );
                strcat( tellMsg, " told you ***: " );
                strcat( tellMsg, mesgTable[srcId][destId].message[i] );
                write( STDOUT_FILENO, tellMsg, strlen( tellMsg ) );
                write( STDOUT_FILENO, "\n", 1 );
                memset( mesgTable[srcId][destId].message[i], '\0', strlen( mesgTable[srcId][destId].message[i] ) );
            }
            mesgTable[srcId][destId].numMesg = 0;
        }
    }
}


// print the error messages
void err_dump( const char* x ){
    perror(x);
    exit(1);
}

// create shell and listen commands
void shell_loop( int ser_sockfd ){
    char *line;
    char *tmp;
    char **args;
    char **commands;

    int status = 1;
    int commandNum;
    int pipeTable[MAX_PIPE_NUM][3] = {0};
    int pipeCounter = 0;
    int sameCounterRow = -1;
    int insertPipeRow = -1;
    int pipefd[2];
    int zeroCounterRow = -1;
    int errNum = 0;
    int exclamation = 0;

    initialPipeTable( pipeTable );

    /* initialize the fifoTable */
    for( int i = 0; i < 30; i++ ){
        for( int j = 0; j < 30; j++ ){
            char a[20];
            char writefifoName[20];
            char readfifoName[20];
            sprintf( a, "%d", i );
            strcpy( writefifoName, "/tmp/" );
            strcat( writefifoName, a );
            strcat( writefifoName, "_to_");
            sprintf( a, "%d", j );
            strcat( writefifoName, a );
            fifoTable[i][j].writeNum = -1;
            // strcpy( readfifoName, a );
            // strcat( readfifoName, "_to_" );
            // sprintf( a, "%d", i );
            // strcat( readfifoName, a );

            strcpy( fifoTable[i][j].writefifo, writefifoName );
            // strcpy( fifoTable[i][j].readfifo, readfifoName );

            /* create the FIFO */
            mkfifo( fifoTable[i][j].writefifo, 0666 );
            fifoTable[i][j].readNum = open( fifoTable[i][j].writefifo, O_RDONLY|O_NONBLOCK );
        }
    }


    while( status ){

        write( ser_sockfd, "% ", 2 );

        // substact one to every counter in table
        subOneToCounter( pipeTable );

        // find the zero counter
        zeroCounterRow = findSameCounter( pipeTable, 0 );

        line = shell_read_line( ser_sockfd );
        strcpy( com, line );
        for( int j = 0; j < 1024; j++ ){
            if( com[j] == '\n' || com[j] == '\r' ){
                com[j] = '\0';
            }
        }

        // when shell_read_line doesn't read data
        if( strlen(line) <= 2 ){
            continue;
        }

        // parse commands with pipe symbol
        commands = shell_split_pipe( line );
        commandNum = calArgsLen( commands );

        errNum = findErrNum( commands, commandNum );

        if( errNum != 0 ){
            exclamation = 1;
        }
        else{
            exclamation = 0;
        }

        if( commandNum > 2 ){
            pipeCounter = containNum( commands, commandNum );
            if( pipeCounter != 0 ){
                // create pipe and put it to table
                sameCounterRow = findSameCounter( pipeTable, pipeCounter );
                if( sameCounterRow != -1 ){
                    // put it to the same pipe
                    status = dealpipe( commands, commandNum - 1, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                }
                else{
                    insertPipeRow = discoverEmptyEntryInTable( pipeTable );
                    // put the pipe inside table
                    if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                    pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                    pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                    pipeTable[insertPipeRow][2] = pipeCounter;
                    status = dealpipe( commands, commandNum - 1, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                }
            }
            else{
                // deal with no pipe numbers
                if( errNum != 0 ){
                    sameCounterRow = findSameCounter( pipeTable, errNum );
                    if( sameCounterRow != -1 ){
                        status = dealpipe( commands, commandNum - 1, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                    }
                    else{
                        insertPipeRow = discoverEmptyEntryInTable( pipeTable );

                        // put the pipe inside table
                        if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                        pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                        pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                        pipeTable[insertPipeRow][2] = errNum;
                        status = dealpipe( commands, commandNum - 1, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                    }
                }
                else{
                    status = dealpipe( commands, commandNum, pipeTable, -1, zeroCounterRow, exclamation );
                }
            }
        }
        else if( commandNum == 2 ){
            if( zeroCounterRow == -1 ){
                if( errNum != 0 ){
                    sameCounterRow = findSameCounter( pipeTable, errNum );
                    if( sameCounterRow != -1 ){
                        status = dealpipe( commands, commandNum, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                    }
                    else{
                        insertPipeRow = discoverEmptyEntryInTable( pipeTable );

                        // put the pipe inside table
                        if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                        pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                        pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                        pipeTable[insertPipeRow][2] = errNum;
                        status = dealpipe( commands, commandNum, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                    }
                }
                else{
                    status = dealpipe( commands, commandNum, pipeTable, -1, zeroCounterRow, exclamation );
                }
            }
            else{

                if( errNum != 0 ){
                    sameCounterRow = findSameCounter( pipeTable, errNum );
                    if( sameCounterRow != -1 ){
                        status = dealpipe( commands, commandNum, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                    }
                    else{
                        insertPipeRow = discoverEmptyEntryInTable( pipeTable );

                        // put the pipe inside table
                        if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                        pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                        pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                        pipeTable[insertPipeRow][2] = errNum;
                        status = dealpipe( commands, commandNum, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                    }
                }
                else{
                    status = dealpipe( commands, commandNum, pipeTable, -1, zeroCounterRow, exclamation );
                }
            }
        }

        if( closefifo->flag == 0 ){
            close( fifoTable[closefifo->i][closefifo->j].readNum );
            unlink( fifoTable[closefifo->i][closefifo->j].writefifo );
            mkfifo( fifoTable[closefifo->i][closefifo->j].writefifo, 0666 );
            fifoTable[closefifo->i][closefifo->j].readNum = open( fifoTable[closefifo->i][closefifo->j].writefifo, O_RDONLY|O_NONBLOCK );
            closefifo->flag = 1;
        }
    }
}

// read the input commands
char *shell_read_line( int ser_sockfd ){
    char *line = NULL;
    ssize_t bufsize = 0;
    FILE *fp = fdopen( ser_sockfd, "r" ); /* convert file descriptor into FILE pointer (getline needs) */
    getline( &line, &bufsize, fp );
    // free( fp )
    return line;
}

char **shell_split_pipe( char *line ){
    int bufsize = SHELL_TOK_BUFSIZE, position = 0;
    char **tokens = (char**)malloc( bufsize * sizeof(char*) );
    char *token;

    if( !tokens ){
        err_dump( "shell_split_pipe: allocation error" );
    }

    /* split by pipe symbol */
    token = strtok( line, "|" );

    while( token != NULL ){
        tokens[position] = token;

        position++;

        if( position >= bufsize ){
            bufsize += SHELL_TOK_BUFSIZE;
            tokens = realloc( tokens, bufsize * sizeof(char*) );
            if( !tokens ){
                err_dump("shell_split_line: allocation error");
            }
        }

        token = strtok( NULL, "|" );
    }

    tokens[position] = NULL;

    return tokens;
}

/* Split the line user inputs. */
char **shell_split_line( char *line ){
    int bufsize = SHELL_TOK_BUFSIZE, position = 0;
    char **tokens = (char**)malloc( bufsize * sizeof(char*) );
    char *token;

    if( !tokens ){
        err_dump( "shell_split_line: allocation error" );
    }

    /* split by SHELL_TOK_DELIM */
    token = strtok( line, SHELL_TOK_DELIM );
    while( token != NULL ){
        tokens[position] = token;
        position++;

        if( position >= bufsize ){
            bufsize += SHELL_TOK_BUFSIZE;
            tokens = realloc( tokens, bufsize * sizeof(char*) );
            if( !tokens ){
                err_dump("shell_split_line: allocation error");
            }
        }
        token = strtok( NULL, SHELL_TOK_DELIM );
    }

    tokens[position] = NULL;

    return tokens;
}

int shell_execute( char **args ){

    if( strcmp( args[0], "setenv" ) == 0 ){
        return setenvPATH( args );
    }

    if( strcmp( args[0], "printenv" ) == 0 ){
        return printenv( args );
    }

    if( strcmp( args[0], "exit" ) == 0 ){
        // sending leaving messages
        char leavingMsg[1024] = "\0";
        strcat( leavingMsg, "*** User \'" );
        for( int i = 0; i < 30; i++ ){
            if( userinfo[i].pid == getppid() ){
                strcat( leavingMsg, userinfo[i].nickname );
                strcat( leavingMsg, "\' left" );
                strcat( leavingMsg, ". ***\n" );


                /* delete from userinfo */
                userinfo[i].id = 0;
                memset( userinfo[i].nickname, '\0', sizeof( userinfo[i].nickname ) );
                memset( userinfo[i].ip, '\0', sizeof( userinfo[i].ip ) );


                for( int j = 0; j < 30; j++ ){
                    fifoTable[i][j].writeNum = -1;
                    fifoTable[j][i].writeNum = -1;
                }
                break;
            }
        }
        strcpy( sysMesg, leavingMsg );

        for( int i = 0; i < 30; i++ ){
            if( userinfo[i].pid != 0 ){
                kill( userinfo[i].pid, SIGUSR1 );
            }
        }

        return 0;
    }

    if( strcmp( args[0], "who" ) == 0 ){
        write( STDOUT_FILENO, "<ID>\t", strlen("<ID>\t") );
        write( STDOUT_FILENO, "<nickname>\t", strlen("<nickname>\t") );
        write( STDOUT_FILENO, "<IP/port>\t", strlen("<IP/port>\t") );
        write( STDOUT_FILENO, "<indicate me>", strlen("<indicate me>") );
        write( STDOUT_FILENO, "\n", 1 );
        for( int i = 0; i < 30; i++ ){
            if( userinfo[i].id != 0 ){
                char a[1000];
                sprintf( a, "%d", userinfo[i].id );
                write( STDOUT_FILENO, a, strlen(a) );
                write( STDOUT_FILENO, "\t", 1 );
                write( STDOUT_FILENO, userinfo[i].nickname, strlen( userinfo[i].nickname ) );
                write( STDOUT_FILENO, "\t", 1 );
                write( STDOUT_FILENO, userinfo[i].ip, strlen( userinfo[i].ip ) );
                if( getppid() == userinfo[i].pid ){
                    write( STDOUT_FILENO, "\t", 1 );
                    write( STDOUT_FILENO, "<-me", strlen("<-me") );
                }
                write( STDOUT_FILENO, "\n", 1 );
            }
        }
        return 1;
    }

    if( strcmp( args[0], "name" ) == 0 ){
        char errMsg[1024] = "\0";
        strcpy( errMsg, "*** User \'" );
        for( int i = 0; i < 30; i++ ){
            if( strcmp( userinfo[i].nickname, args[1] ) == 0 ){
                strcat( errMsg, args[1] );
                strcat( errMsg, "\' already exists. ***\n" );
                strcpy( sysMesg, errMsg );

                for( int j = 0; j < 30; j++ ){
                    if( userinfo[j].pid == getppid() ){
                        kill( userinfo[j].pid, SIGUSR1 );
                        return 1;
                    }
                }
            }
        }


        for( int i = 0; i < 30; i++ ){
            if( userinfo[i].pid == getppid() ){
                strcpy( userinfo[i].nickname, args[1] );
                char changeNameMsg[1024] = "\0";
                strcat( changeNameMsg, "*** User from " );
                strcat( changeNameMsg, userinfo[i].ip );
                strcat( changeNameMsg, " is named \'" );
                strcat( changeNameMsg, args[1] );
                strcat( changeNameMsg, "\'. ***\n" );
                strcpy( sysMesg, changeNameMsg );

                for( int j = 0; j < 30; j++ ){
                    if( userinfo[j].pid != 0 ){
                        kill( userinfo[j].pid, SIGUSR1 );
                    }
                }
                break;
            }
        }
        return 1;
    }

    if( strcmp( args[0], "tell" ) == 0 ){
        int destCli = atoi( args[1] );
        int srcCli = 0;
        for( int i = 0; i < 30; i++ ){
            if( userinfo[i].pid == getppid() ){
                srcCli = i;
                break;
            }
        }

        int n = 0;
        while( args[n + 2] != NULL ){
            n++;
        }

        char message[1024];
        memset( message, '\0', sizeof( message ) );
        for( int i = 2; i < n + 2; i++ ){
            if( i == 2 ){
                strcpy( message, args[i] );
            }
            else{
                strcat( message, args[i] );
            }

            if( i != n + 1 ){
                strcat( message, " " );
            }
        }

        char errMsg[1024] = "\0";
        strcpy( errMsg, "*** Error: user #" );
        if( userinfo[destCli - 1].id == 0 ){
            char a[30];
            sprintf( a, "%d", destCli );
            strcat( errMsg, a );
            strcat( errMsg, " does not exist yet. ***\n" );
            strcpy( sysMesg, errMsg );
            kill( userinfo[srcCli].pid, SIGUSR1 );
            return 1;
        }


        int messageIndex = mesgTable[srcCli][destCli - 1].numMesg;
        strcpy( mesgTable[srcCli][destCli - 1].message[messageIndex], message );
        mesgTable[srcCli][destCli - 1].numMesg++;

        idinfo->srcId = srcCli;
        idinfo->destId = destCli - 1;

        kill( userinfo[destCli-1].pid, SIGUSR2 );
        return 1;
    }

    if( strcmp( args[0], "yell" ) == 0 ){
        int n = 0;
        int srcCli = 0;

        while( args[n + 1] != NULL ){
            n++;
        }

        char message[1024];
        memset( message, '\0', sizeof( message ) );
        for( int i = 1; i < n + 1; i++ ){
            if( i == 1 ){
                strcpy( message, args[i] );
            }
            else{
                strcat( message, args[i] );
            }

            if( i != n + 1 ){
                strcat( message, " " );
            }
        }

        for( int i = 0; i < 30; i++ ){
            if( userinfo[i].pid == getppid() ){
                for( int j = 0; j < 30; j++ ){
                    if( userinfo[j].id != 0 ){
                        mesgTable[i][j].broadFlag = 1;

                        int messageIndex = mesgTable[i][j].numBroad;
                        strcpy( mesgTable[i][j].broadcast[messageIndex], message );
                        mesgTable[i][j].numBroad++;
                        idinfo->srcId = i;
                        idinfo->destId = j;
                        kill( userinfo[j].pid, SIGUSR2 );
                    }
                }
                break;
            }
        }
        return 1;
    }

    return shell_launch_command( args );
}

int shell_launch_command( char **args ){
    pid_t pid, wpid;
    int status;

    int inputfifoIdx = -1;
    int outputfifoIdx = -1;
    int inputfifoNum = -1;
    int outputfifoNum = -1;
    int sourceId = 0;
    int argsNum = calArgsLen( args );
    int insertNullIdx = 0;
    char errMesg[1024];
    char preStr1[5];
    char preStr2[5];

    for( int j = 0; j < argsNum - 1; j++ ){
        if( args[j][0] == '>' && strlen( args[j] ) >= 2 ){
            outputfifoIdx = j;
            strcpy( preStr1, args[j] );
            memmove( &args[j][0], &args[j][1], strlen( args[j] ) );
            outputfifoNum = atoi( args[j] ) - 1;
        }
        if( args[j][0] == '<' && strlen( args[j] ) >= 2 ){
            inputfifoIdx = j;
            strcpy( preStr2, args[j] );
            memmove( &args[j][0], &args[j][1], strlen( args[j] ) );
            inputfifoNum = atoi( args[j] ) - 1;
        }
    }

    for( int i = 0; i < 30; i++ ){
        if( userinfo[i].pid == getppid() ){
            // sourceId = userinfo[i].id;
            sourceId = i;
        }
    }

    // duplicate the stdout or stdin by the fifo
    if( outputfifoNum != -1 ){
        // mkfifo( fifoTable[sourceId][outputfifoNum].writefifo, 0666 );
        // fifoTable[sourceId][outputfifoNum].readNum = open( fifoTable[sourceId][outputfifoNum].writefifo, O_RDWR );
        // fifoTable[sourceId][outputfifoNum].readNum = open( fifoTable[sourceId][outputfifoNum].writefifo, O_RDONLY|O_NONBLOCK );
        // char bbb[5];
        // sprintf( bbb, "%d", fifoTable[sourceId][outputfifoNum].writeNum );
        // write( STDOUT_FILENO, bbb, strlen(bbb) );
        if( fifoTable[sourceId][outputfifoNum].writeNum != -1 ){
            // char errMesg[1024];
            strcpy( errMesg, "*** Error: the pipe #" );
            char aaa[30];
            sprintf( aaa, "%d", sourceId + 1 );
            strcat( errMesg, aaa );
            strcat( errMesg, "->#" );
            sprintf( aaa, "%d", outputfifoNum + 1 );
            strcat( errMesg, aaa );
            strcat( errMesg, " already exists. ***\n" );
            strcpy( sysMesg, errMesg );
            kill( userinfo[sourceId].pid, SIGUSR1 );
            return 1;
        }

        if( userinfo[outputfifoNum].id == 0 ){
            strcpy( errMesg, "*** Error: user #" );
            char ccc[30];
            sprintf( ccc, "%d", outputfifoNum + 1 );
            strcat( errMesg, ccc );
            strcat( errMesg, " does not exist yet. ***\n" );
            strcpy( sysMesg, errMesg );
            kill( userinfo[sourceId].pid, SIGUSR1 );
            return 1;
        }
        fifoTable[sourceId][outputfifoNum].writeNum = open( fifoTable[sourceId][outputfifoNum].writefifo, O_WRONLY );
        // close( fifoTable[sourceId][outputfifoNum].readNum );


    }
    else if( inputfifoNum != -1 ){
        if( fifoTable[inputfifoNum][sourceId].writeNum == -1 ){
            strcpy( errMesg, "*** Error: the pipe #" );
            char aaa[30];
            sprintf( aaa, "%d", inputfifoNum + 1 );
            strcat( errMesg, aaa );
            strcat( errMesg, "->#" );
            sprintf( aaa, "%d", sourceId + 1 );
            strcat( errMesg, aaa );
            strcat( errMesg, " does not exist yet. ***\n" );
            strcpy( sysMesg, errMesg );
            kill( userinfo[sourceId].pid, SIGUSR1 );
            return 1;
        }
        fifoTable[inputfifoNum][sourceId].readNum = open( fifoTable[inputfifoNum][sourceId].writefifo, O_RDONLY|O_NONBLOCK );
        // fifoTable[inputfifoNum][sourceId].writeNum = open( fifoTable[inputfifoNum][sourceId].writefifo, O_WRONLY|O_NONBLOCK );
        // fifoTable[inputfifoNum][sourceId].readNum = open( fifoTable[inputfifoNum][sourceId].writefifo, O_RDONLY|O_NONBLOCK );
        // dup2( STDIN_FILENO, fifoTable[inputfifoNum][sourceId].readNum );
        // close( fifoTable[inputfifoNum][sourceId].writeNum );
    }

    pid = fork();
    if( pid == 0 ){
        char pipeMsg[1024];
        int index = 0;
        while( args[index] != NULL ){
            if( strcmp( args[index], ">" ) == 0 ){
                redirection( args, index + 1 );
                return 1;
            }
            index++;
        }

        if( inputfifoNum != -1 ){
            // fifoTable[inputfifoNum][sourceId].readNum = open( fifoTable[inputfifoNum][sourceId].writefifo, O_RDONLY|O_NONBLOCK );
            // char a[30];
            // sprintf( a, "%d", fifoTable[inputfifoNum][sourceId].readNum );
            // write( STDOUT_FILENO, a, strlen(a) );
            // write( STDOUT_FILENO, fifoTable[inputfifoNum][sourceId].writefifo, strlen(fifoTable[inputfifoNum][sourceId].writefifo) );

            strcpy( pipeMsg, "*** " );
            strcat( pipeMsg, userinfo[sourceId].nickname );
            strcat( pipeMsg, " (#");
            char b[30];
            sprintf( b, "%d", userinfo[sourceId].id );
            strcat( pipeMsg, b );
            strcat( pipeMsg, ") just received from " );
            strcat( pipeMsg, userinfo[inputfifoNum].nickname );
            strcat( pipeMsg, " (#");
            sprintf( b, "%d", userinfo[inputfifoNum].id );
            strcat( pipeMsg, b );
            strcat( pipeMsg, ") by \'" );
            // for( int i = 0; i < argsNum - 1; i++ ){
            //     if( i != argsNum - 2 ){
            //         if( i == inputfifoIdx ){
            //             strcat( pipeMsg, preStr2 );
            //             strcat( pipeMsg, " " );
            //         }
            //         else if( i == outputfifoIdx ){
            //             strcat( pipeMsg, preStr1 );
            //             strcat( pipeMsg, " " );
            //         }
            //         else{
            //             strcat( pipeMsg, args[i] );
            //             strcat( pipeMsg, " " );
            //         }
            //     }
            //     else{
            //         if( i == inputfifoIdx ){
            //             strcat( pipeMsg, preStr2 );
            //         }
            //         else if( i == outputfifoIdx ){
            //             strcat( pipeMsg, preStr1 );
            //         }
            //         else{
            //             strcat( pipeMsg, args[i] );
            //         }
            //     }
            // }
            strcat( pipeMsg, com );
            strcat( pipeMsg, "\' ***\n");
            strcpy( sysMesg, pipeMsg );
            for( int i = 0; i < 30; i++ ){
                if( userinfo[i].pid != 0 ){
                    kill( userinfo[i].pid, SIGUSR1 );
                }
            }



            dup2( fifoTable[inputfifoNum][sourceId].readNum, STDIN_FILENO );
            // write( STDOUT_FILENO, buf, strlen(buf) );

            // close( fifoTable[inputfifoNum][sourceId].writeNum );
        }

        if( outputfifoNum != -1 ){
            // fifoTable[sourceId][outputfifoNum].writeNum = open( fifoTable[sourceId][outputfifoNum].writefifo, O_WRONLY|O_NONBLOCK );
            // fifoTable[sourceId][outputfifoNum].writeNum = open( fifoTable[sourceId][outputfifoNum].writefifo, O_RDWR );
            // char a[30];
            // sprintf( a, "%d", fifoTable[sourceId][outputfifoNum].writeNum );
            // write( STDOUT_FILENO, fifoTable[sourceId][outputfifoNum].writefifo, strlen(fifoTable[sourceId][outputfifoNum].writefifo) );
            // close( fifoTable[sourceId][outputfifoNum].readNum );

            strcpy( pipeMsg, "*** " );
            strcat( pipeMsg, userinfo[sourceId].nickname );
            strcat( pipeMsg, " (#");
            char b[30];
            sprintf( b, "%d", userinfo[sourceId].id );
            strcat( pipeMsg, b );
            strcat( pipeMsg, ") just piped \'" );
            // for( int i = 0; i < argsNum - 1; i++ ){
            //     if( i != argsNum - 2 ){
            //         if( i == outputfifoIdx ){
            //             strcat( pipeMsg, preStr1 );
            //             strcat( pipeMsg, " " );
            //         }
            //         else if( i == inputfifoIdx ){
            //             strcat( pipeMsg, preStr2 );
            //             strcat( pipeMsg, " " );
            //         }
            //         else{
            //             strcat( pipeMsg, args[i] );
            //             strcat( pipeMsg, " " );
            //         }
            //     }
            //     else{
            //         if( i == outputfifoIdx ){
            //             strcat( pipeMsg, preStr1 );
            //         }
            //         else if( i == inputfifoIdx ){
            //             strcat( pipeMsg, preStr2 );
            //         }
            //         else{
            //             strcat( pipeMsg, args[i] );
            //         }
            //     }
            // }
            strcat( pipeMsg, com );
            strcat( pipeMsg, "\' to " );
            strcat( pipeMsg, userinfo[outputfifoNum].nickname );
            strcat( pipeMsg, " (#");
            sprintf( b, "%d", userinfo[outputfifoNum].id );
            strcat( pipeMsg, b );
            strcat( pipeMsg, ") ***\n" );
            strcpy( sysMesg, pipeMsg );
            for( int i = 0; i < 30; i++ ){
                if( userinfo[i].pid != 0 ){
                    kill( userinfo[i].pid, SIGUSR1 );
                }
            }

            dup2( fifoTable[sourceId][outputfifoNum].writeNum, STDOUT_FILENO );
            dup2( fifoTable[sourceId][outputfifoNum].writeNum, STDERR_FILENO );


            // char buf[1024];
            // read( r, buf, 5 );
            // write( STDOUT_FILENO, buf, strlen( buf ) );
            // close( fifoTable[sourceId][outputfifoNum].readNum );
        }


        if( outputfifoIdx != 0 ){
            args[outputfifoIdx] = NULL;
        }

        if( inputfifoIdx != 0 ){
            args[inputfifoIdx] = NULL;
        }

        /* child process */
        if( execvp( args[0], args ) == -1 ){
            write( STDERR_FILENO, "Unknown command: [", strlen("Unknown command: [") );
            write( STDERR_FILENO, args[0], strlen(args[0]) );
            write( STDERR_FILENO, "].\n", 3 );
            exit(-1);
        }

        exit( EXIT_FAILURE );
    }
    else if( pid < 0){
        /* fork failed */
        err_dump("shell_launch_command: fork failed");
    }
    else{
        if( outputfifoNum != -1 ){
            close( fifoTable[sourceId][outputfifoNum].writeNum );
            // close( fifoTable[sourceId][outputfifoNum].readNum );
            // char a[30];
            // sprintf( a, "%d", fifoTable[sourceId][outputfifoNum].readNum );
            // write( STDOUT_FILENO, a, strlen(a) );
            // write( STDOUT_FILENO, "HELLO", 5 );
            // unlink( fifoTable[sourceId][outputfifoNum].writefifo );
        }

        if( inputfifoNum != -1 ){
            // close( fifoTable[inputfifoNum][sourceId].writeNum );

            close( fifoTable[inputfifoNum][sourceId].readNum );
            // unlink( fifoTable[inputfifoNum][sourceId].writefifo );
        }
        /* parent process */
        do{
            wpid = waitpid( pid, &status, WUNTRACED );
        } while( !WIFEXITED(status) && !WIFSIGNALED(status) );

        if( inputfifoNum != -1 ){
            fifoTable[inputfifoNum][sourceId].writeNum = -1;
            closefifo->i = inputfifoNum;
            closefifo->j = sourceId;
            closefifo->flag = 0;
        }

        // char ccc[30];
        // sprintf( ccc, "%d", status );
        // write( STDOUT_FILENO, ccc, strlen(ccc) );
        if( status != 0 ){
            return 3;
        }
    }

    return 1;
}

/* Builtin function setenv */
int setenvPATH( char **args ){
    int n = strlen( args[1] ) + strlen( args[2] );
    char *env = (char*)malloc( sizeof(char) * n + 2 );
    memset( env, '\0', n + 2 );

    strcat( env, args[1] );
    strcat( env, "=" );
    strcat( env, args[2] );

    // printf( "%s", args[2] );
    putenv( env );

    return 2;
}

/* Builtin function printenv */
int printenv( char **args ){

    // print the env which user indicates
    write( STDOUT_FILENO, "PATH=", 5 );
    write( STDOUT_FILENO, getenv( args[1] ) , strlen( getenv( args[1] ) ) );
    write( STDOUT_FILENO, "\n", 1 );

    return 1;
}

void redirection( char **args, int filename_pos ){
    FILE *newfile;
    newfile = fopen( args[filename_pos], "w" );

    // move command to new array
    int argsLen = calArgsLen( args );
    char **command = (char**)malloc( sizeof(char*) * argsLen - 2 );
    for( int i = 0; i < ( argsLen - 3 ); i++ ){
        command[i] = args[i];
    }
    // write( STDOUT_FILENO, command[0], strlen( command[0] ) );
    command[ argsLen - 3 ] = NULL;

    dup2( fileno( newfile ), STDOUT_FILENO );

    if( execvp( command[0], command ) == -1 ){
        write( STDERR_FILENO, "Unknown command: [", strlen("Unknown command: [") );
        write( STDERR_FILENO, command[0], strlen(command[0]) );
        write( STDERR_FILENO, "].\n", 3 );
    }

    fclose( newfile );

    return;
}

/* calculate the amount of the elements in two dimensional pointer */
int calArgsLen( char **args ){
    int n = 0;
    while( args[n] != NULL ){
        n++;
    }

    return ( n + 1 );
}

/* deal with original pipe */
int dealpipe( char **commands, int commandNum, int table[][3], int tableRow, int zeroCounterRow, int exclamation ){
    int status = 1;
    int pid, pipefd[2];
    int passNum = 0;
    int fd_in = 0;
    int flag_to_process_number = tableRow;
    char **showMsg;

    if( zeroCounterRow != -1 ){
        fd_in = table[zeroCounterRow][1];
        close( table[zeroCounterRow][0] );
    }

    // parse every command in commands array
    for( int i = 0; i < commandNum - 1; i++ ){
        char **args;
        args = shell_split_line( commands[i] );

        int len = calArgsLen( args );
        if( args[len - 2][0] == '!' ){
            args[len - 2] = NULL;
        }

        if( pipe(pipefd) < 0 ) err_dump("dealpipe: can't create pipes");

        pid = fork();

        if( (pid) < 0 ){
            err_dump("dealpip: can't fork");
        }
        else if( pid == 0 ){
            close( pipefd[0] );

            dup2( fd_in, STDIN_FILENO );

            if( i != commandNum - 2 ){
                dup2( pipefd[1], STDOUT_FILENO );
            }
            else if( i == commandNum - 2 ){
                if( ( flag_to_process_number != -1 )  ){
                    if( exclamation == 1 ){
                        dup2( table[tableRow][0], STDERR_FILENO );
                    }
                    dup2( table[tableRow][0], STDOUT_FILENO );
                }
            }

            status = shell_execute( args );

            exit(status);
        }
        else{
            close( pipefd[1] );

            wait( &status );

            if( i > 0 ){
                close( fd_in );
            }

            fd_in = pipefd[0];

            if( status == 0 ){
                break;
            }
            else if( status == 512 ){
                status = shell_execute( args );
            }
            else if( status == 768 ){
                status = 1;
                break;
            }
        }
    }

    if( zeroCounterRow != -1 ){
        close( table[zeroCounterRow][1] );
        table[zeroCounterRow][2] = -1;
    }
    close( pipefd[0] );
    return status;
}

// initialize the pipe table
void initialPipeTable( int table[][3] ){
    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        table[i][2] = -1;
    }
}

// get the pipe number if it has
int containNum( char **commands, int commandNum ){
    char **args;
    int counter = 0;

    if( counter = atoi( commands[commandNum - 2] ) ){
        return counter;
    }

    return 0;
}

int findErrNum( char **commands, int commandNum ){
    char *line = (char*)malloc( strlen(commands[commandNum - 2]) * sizeof(char) + 1 );
    char **args;

    memcpy( line, commands[commandNum - 2], strlen( commands[commandNum - 2] ) + 1 );

    args = shell_split_line( line );

    int len = calArgsLen( args );
    int counter = 0;
    char *num = (char*)malloc( sizeof(char) * strlen( args[len - 2] ) );

    if( args[len - 2][0] == '!' ){
        memmove( &args[len - 2][0], &args[len - 2][1], strlen( args[len - 2] ) );
        counter = atoi( args[len - 2] );
        return counter;
    }

    free( line );
    return counter;
}


// return the first counter which is -1
int discoverEmptyEntryInTable( int table[][3] ){
    int row = -1;

    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        if( table[i][2] == -1 ){
            row = i;
            return row;
        }
    }
    return row;
}

// return the row of the same counter
int findSameCounter( int table[][3], int counter ){
    int row = -1;

    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        if( counter == table[i][2] ){
            row = i;
            return row;
        }
    }

    return row;
}

// substract every counter in the table
void subOneToCounter( int table[][3] ){
    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        if( table[i][2] >= 0 ){
            table[i][2]-=1;
        }
    }
}
